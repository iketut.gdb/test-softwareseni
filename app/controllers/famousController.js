const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
FamousPeople = mongoose.model('FamousPeople');
var cors = require('cors');

module.exports = (app) => {
  app.use('/', router);
};

router.get('/api/v1/famous-people',cors(), function(req, res, next){
    var response = {
      "error" : false,
      "data" : [
        {
          name: "Bill Gates",
          age: 61,
          company: "Microsoft"
        },
        {
          name: "Steve Jobs",
          age: 56,
          company: "Apple"
        },
        {
          name: "Gabe Newell",
          age: 53,
          company: "Valve"
        }
      ]
    }
    res.status(200).json({response});
  });

  
// router.post('/register', cors(),function (req, res, next) {
//     var data = new FamousPeople ({  name: req.body.name,
//                             age: req.body.age,
//                             company: req.body.company
//                         })
//     data.save(function(err,result){
//       if(err){
//         console.log(err);
//       }else{
//         res.status(200).json({result})
//       }
//     })
// });

