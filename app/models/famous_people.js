

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FamousPeopleSchema = new Schema({
  name: String,
  age: Number,
  company: String
});

FamousPeopleSchema.virtual('date')
  .get(() => this._id.getTimestamp());

mongoose.model('FamousPeople', FamousPeopleSchema);

