

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  title: String,
  user: String,
});

UserSchema.virtual('date')
  .get(() => this._id.getTimestamp());

mongoose.model('User', UserSchema);