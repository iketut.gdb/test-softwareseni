

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectSchema = new Schema({
  first_name: String,
  last_name: String,
});

ProjectSchema.virtual('date')
  .get(() => this._id.getTimestamp());

mongoose.model('Project', ProjectSchema);