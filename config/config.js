const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'expressapi'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/expressapi-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'expressapi'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/expressapi-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'expressapi'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/expressapi-production'
  }
};

module.exports = config[env];
